﻿using UnityEngine;

namespace TheMTC.Adv_Vis.UX
{
    public class RotationSphereRig : MtcObjectManipulatorState
    {  
        [SerializeField]
        private GameObject target;
        public GameObject Target
        {
            get
            {
                return target;
            }

            set
            {
                foreach (var gizmo in rotationGizmos)
                {
                    gizmo.Target = value;
                }
                target = value;
            }
        }

        

        public RotationGizmo[] rotationGizmos;


        //private Vector3 initialHandPosition;
        //private Quaternion initialTarjetOrientation;
        //private Quaternion initialRigOrientation;
        //private Vector3 lastHandWorldPos = Vector3.zero;
        //private InputEventData inputDownEventData;
        //private SourceStateEventData inputDetectedEventData;


        //public enum RotationAxis  {X, Y, Z, None};
        //public RotationAxis FixedAxis;

        private Space space;
        public Space Space { get => space;
            set
            {
                foreach (var gizmo in rotationGizmos)
                {
                    gizmo.Space = value;
                }
                space = value;
            }
        }



        //private Vector3 GetHandPosition(uint sourceId)
        //{
        //    Vector3 handPosition = new Vector3(0, 0, 0);
        //    inputDownEventData.InputSource.TryGetGripPosition(sourceId, out handPosition);
        //    return handPosition;
        //}



        //void IInputHandler.OnInputDown(InputEventData eventData)
        //{
        //    inputDownEventData = eventData;

        //    initialTarjetOrientation = Target.transform.rotation;

        //    initialRigOrientation = transform.rotation;


        //    //initial Handle Gizmo
        //    InitialHandleOnRigPosition = Handle.HandleOnRig.position;
        //    //InitialHandleOnRigRotation = HandleOnRig.transform.rotation;

        //    InitialHandleOnHandPosition = GetHandPosition(inputDownEventData.SourceId);
        //    switch (FixedAxis)
        //    {
        //        case RotationAxis.X:
        //            InitialHandleOnHandPosition = new Vector3(transform.position.x, InitialHandleOnHandPosition.y, InitialHandleOnHandPosition.z);
        //            break;
        //        case RotationAxis.Y:
        //            InitialHandleOnHandPosition = new Vector3(InitialHandleOnHandPosition.x, transform.position.y, InitialHandleOnHandPosition.z);
        //            break;
        //        case RotationAxis.Z:
        //            InitialHandleOnHandPosition = new Vector3(InitialHandleOnHandPosition.x, InitialHandleOnHandPosition.y, transform.position.z);
        //            break;
        //        case RotationAxis.None:
        //            break;
        //        default:
        //            break;
        //    }


        //    HoloToolkit.Unity.InputModule.InputManager.Instance.PushModalInputHandler(gameObject);

        //    eventData.Use();
        //}

        //public void OnInputUp(InputEventData eventData)
        //{
        //    inputDownEventData = null;
        //    ResetRigHandles();

        //    if (eventData != null)
        //    {
        //        eventData.Use();
        //    }
        //}

        //void ISourceStateHandler.OnSourceDetected(SourceStateEventData eventData)
        //{
        //    inputDetectedEventData = eventData;
        //}

        //void ISourceStateHandler.OnSourceLost(SourceStateEventData eventData)
        //{
        //    if ((inputDownEventData != null) &&
        //    (eventData.SourceId == inputDownEventData.SourceId))
        //    {
        //        inputDownEventData = null;
        //        ResetRigHandles();
        //        eventData.Use();
        //    }

        //    if (inputDetectedEventData != null)
        //    {
        //        inputDetectedEventData = null;
        //    }
        //}

        //private void ResetRigHandles()
        //{
        //    inputDownEventData = null;

        //    HoloToolkit.Unity.InputModule.InputManager.Instance.PopModalInputHandler();
        //}


        //handles
        public Transform OnRigTransform;

        [Header("Handle")]
        public RotationGizmoHandle Handle;

        //InitialHandle
        [Header("Initial Handle")]
        public RotationGizmoHandle InitialHandle;
        private Vector3 InitialHandleOnRigPosition;
        private Vector3 InitialHandleOnHandPosition;

        public override void StateEnter()
        {
            base.StateEnter();

            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.Rotation);
             
        }

        public override void StateExit()
        {
            base.StateExit();

            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.none);
        }

        public override void UpdateState()
        {
            base.UpdateState();

            this.transform.position = target.transform.position;

            //update initialhandle
            InitialHandle.UpdateHandleElements(InitialHandleOnRigPosition, InitialHandleOnHandPosition);

            if (Space == Space.World)
            {
                transform.eulerAngles = Vector3.zero;
            }
            if (Space == Space.Self)
            {
                transform.rotation = target.transform.rotation;
            }


            //if (inputDownEventData != null)
            //{
            //    Vector3 currentHandPosition = Vector3.zero;
            //    Quaternion currentHandOrientation = Quaternion.identity;

            //    //set values from hand
            //    currentHandPosition = GetHandPosition(inputDownEventData.SourceId);
            //    switch (FixedAxis)
            //    {
            //        case RotationAxis.X:
            //            currentHandPosition = new Vector3(transform.position.x, currentHandPosition.y, currentHandPosition.z);
            //            break;
            //        case RotationAxis.Y:
            //            currentHandPosition = new Vector3(currentHandPosition.x, transform.position.y, currentHandPosition.z);
            //            break;
            //        case RotationAxis.Z:
            //            currentHandPosition = new Vector3(currentHandPosition.x, currentHandPosition.y, transform.position.z);
            //            break;
            //        case RotationAxis.None:
            //            break;
            //        default:
            //            break;
            //    }

            //    //rig faces hand position
            //    transform.LookAt(currentHandPosition, Vector3.up);
            //    //update handle
            //    Handle.UpdateHandleElements(OnRigTransform.position, currentHandPosition);


            //    Quaternion deltaRotation = transform.rotation * Quaternion.Inverse(initialRigOrientation);

            //    //Continuous drag
            //    // TarjetTransform.rotation = deltaRotation * TarjetTransform.rotation;

            //    Target.transform.rotation = deltaRotation * initialTarjetOrientation;




            //    //Save previous Hand Position
            //    lastHandWorldPos = currentHandPosition;
            //}

            //if (inputDetectedEventData != null)
            //{
            //    Vector3 currentHandPosition = Vector3.zero;

            //    inputDetectedEventData.InputSource.TryGetGripPosition(inputDetectedEventData.SourceId, out currentHandPosition);

            //    transform.LookAt(currentHandPosition, Vector3.up);
            //}
        }

    }  

}
