﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RotationGizmoHandle : MonoBehaviour
{
    public Transform HandleOnRig;
    public Transform HandleOnHand;

   

   
   public void UpdateHandleElements(Vector3 OnRigPos, Vector3 OnHandPos)
    {
        HandleOnRig.position = OnRigPos;
        HandleOnHand.position = OnHandPos;

    }


}
