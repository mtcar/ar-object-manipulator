﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TheMTC.Adv_Vis.UX
{
    public class RotationGizmo : MonoBehaviour, IInputHandler, ISourceStateHandler
    {
        [SerializeField]
        private GameObject target;
        public GameObject Target
        {
            get
            {
                return target;
            }

            set
            {
                target = value;
            }
        }


        private Vector3 initialHandPosition;
        private Quaternion initialTarjetOrientation;
        private Quaternion initialRigOrientation;
        private Vector3 lastHandWorldPos = Vector3.zero;
        private InputEventData inputDownEventData;
        private SourceStateEventData inputDetectedEventData;


        public enum RotationAxis { X, Y, Z, None };
        public RotationAxis FixedAxis;

        public Space Space;

        private Vector3 GetHandPosition(uint sourceId)
        {
            Vector3 handPosition = new Vector3(0, 0, 0);
            inputDownEventData.InputSource.TryGetGripPosition(sourceId, out handPosition);
            return handPosition;
        }



        void IInputHandler.OnInputDown(InputEventData eventData)
        {
            inputDownEventData = eventData;

            InitialHandleOnHandPosition = GetHandPosition(inputDownEventData.SourceId);

            initialTarjetOrientation = Target.transform.rotation;

            


            switch (FixedAxis)
            {
                case RotationAxis.X:
                    InitialHandleOnHandPosition = new Vector3(transform.position.x, InitialHandleOnHandPosition.y, InitialHandleOnHandPosition.z);
                    break;
                case RotationAxis.Y:
                    InitialHandleOnHandPosition = new Vector3(InitialHandleOnHandPosition.x, transform.position.y, InitialHandleOnHandPosition.z);
                    break;
                case RotationAxis.Z:
                    InitialHandleOnHandPosition = new Vector3(InitialHandleOnHandPosition.x, InitialHandleOnHandPosition.y, transform.position.z);
                    break;
                case RotationAxis.None:
                    //rig faces hand position
                    transform.LookAt(InitialHandleOnHandPosition, Vector3.up);
                    transform.rotation *= Quaternion.Euler(0, 90, 0);

                    initialRigOrientation = transform.rotation;


                    //initial Handle Gizmo
                    InitialHandleOnRigPosition = Handle.HandleOnRig.position;
                    //InitialHandleOnRigRotation = HandleOnRig.transform.rotation;



                    break;
                default:
                    break;
            }


            HoloToolkit.Unity.InputModule.InputManager.Instance.PushModalInputHandler(gameObject);

            eventData.Use();
        }

        public void OnInputUp(InputEventData eventData)
        {
            inputDownEventData = null;
            ResetRigHandles();

            if (eventData != null)
            {
                eventData.Use();
            }
        }

        void ISourceStateHandler.OnSourceDetected(SourceStateEventData eventData)
        {
            //inputDetectedEventData = eventData;
        }

        void ISourceStateHandler.OnSourceLost(SourceStateEventData eventData)
        {
            if ((inputDownEventData != null) &&
            (eventData.SourceId == inputDownEventData.SourceId))
            {
                inputDownEventData = null;
                ResetRigHandles();
                eventData.Use();
            }

            //if (inputDetectedEventData != null)
            //{
            //    inputDetectedEventData = null;
            //}
        }

        private void ResetRigHandles()
        {
            inputDownEventData = null;

            HoloToolkit.Unity.InputModule.InputManager.Instance.PopModalInputHandler();
        }


        //handles
        public Transform OnRigTransform;

        [Header("Handle")]
        public RotationGizmoHandle Handle;

        //InitialHandle
        [Header("Initial Handle")]
        public RotationGizmoHandle InitialHandle;
        private Vector3 InitialHandleOnRigPosition;
        private Vector3 InitialHandleOnHandPosition;



        void Update()
        {
            //update initialhandle
            //InitialHandle.UpdateHandleElements(InitialHandleOnRigPosition, InitialHandleOnHandPosition);


            if (inputDownEventData != null)
            {
                Vector3 currentHandPosition = Vector3.zero;
                Quaternion currentHandOrientation = Quaternion.identity;

                //set values from hand
                currentHandPosition = GetHandPosition(inputDownEventData.SourceId);
                Debug.Log("FixAxis doesnt worked properly");
                switch (FixedAxis)
                {
                    case RotationAxis.X:
                        currentHandPosition = new Vector3(transform.position.x, currentHandPosition.y, currentHandPosition.z);
                        float deltaRotX = Vector3.SignedAngle(InitialHandleOnHandPosition - target.transform.position, currentHandPosition - target.transform.position, Vector3.right);
                        Debug.Log(deltaRotX);
                        target.transform.rotation = initialTarjetOrientation;
                        target.transform.Rotate(Vector3.right, deltaRotX , Space);
                        break;
                    case RotationAxis.Y:
                        currentHandPosition = new Vector3(currentHandPosition.x, transform.position.y, currentHandPosition.z);
                        float deltaRotY = Vector3.SignedAngle(InitialHandleOnHandPosition - target.transform.position, currentHandPosition - target.transform.position, Vector3.right);
                        Debug.Log(deltaRotY);
                        target.transform.rotation = initialTarjetOrientation;
                        target.transform.Rotate(Vector3.up, deltaRotY, Space);
                        break;
                    case RotationAxis.Z:
                        currentHandPosition = new Vector3(currentHandPosition.x, currentHandPosition.y, transform.position.z);
                        float deltaRotZ = Vector3.SignedAngle(InitialHandleOnHandPosition - target.transform.position, currentHandPosition - target.transform.position, Vector3.right);
                        Debug.Log(deltaRotZ);
                        target.transform.rotation = initialTarjetOrientation;
                        target.transform.Rotate(Vector3.forward, deltaRotZ, Space);
                        break;
                    case RotationAxis.None:

                        //rig faces hand position
                        transform.LookAt(currentHandPosition, Vector3.up);
                        transform.rotation *= Quaternion.Euler(0, 90, 0);
                        //update handle
                        Handle.UpdateHandleElements(OnRigTransform.position, currentHandPosition);


                        Quaternion deltaRotation = transform.rotation * Quaternion.Inverse(initialRigOrientation);


                        //Continuous drag
                        // TarjetTransform.rotation = deltaRotation * TarjetTransform.rotation;

                        Target.transform.rotation = deltaRotation * initialTarjetOrientation;


                        break;
                    default:
                        break;
                }

                //Save previous Hand Position
                lastHandWorldPos = currentHandPosition;
            }

            //if (inputDetectedEventData != null)
            //{
            //    Vector3 currentHandPosition = Vector3.zero;

            //    inputDetectedEventData.InputSource.TryGetGripPosition(inputDetectedEventData.SourceId, out currentHandPosition);

            //    transform.LookAt(currentHandPosition, Vector3.up);
            //}
        }

        private void CalculateArc(float startAngle, float endAngle, float radius, int segments) {
            List<Vector2> arcPoints = new List<Vector2>();
            float angle = startAngle;
            float arcLength = endAngle - startAngle;
            for (int i = 0; i <= segments; i++)
            {
                float x = Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
                float y = Mathf.Cos(Mathf.Deg2Rad * angle) * radius;

                arcPoints.Add(new Vector2(x, y));

                angle += (arcLength / segments);
            }

        }

    }
}
