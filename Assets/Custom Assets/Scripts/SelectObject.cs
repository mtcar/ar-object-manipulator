﻿using UnityEngine;
using HoloToolkit.Unity.InputModule;

namespace TheMTC.Adv_Vis.UX
{
    public class SelectObject : MonoBehaviour, IInputClickHandler
    {
        void IInputClickHandler.OnInputClicked(InputClickedEventData eventData)
        {
            BoundingBoxRig.Instance.SelecObject(gameObject);
        }
    }
}
