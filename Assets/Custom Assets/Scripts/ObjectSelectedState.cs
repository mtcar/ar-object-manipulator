﻿namespace TheMTC.Adv_Vis.UX
{
    public class ObjectSelectedState : MtcObjectManipulatorState
    {
        public override void StateEnter()
        {
            base.StateEnter();

            BoundingBoxRig.Instance.ShowRig = true;
            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.none);
        }

        public override void StateExit()
        {
            base.StateExit();

            BoundingBoxRig.Instance.ShowRig = false;
            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.none);
        }

        public override void UpdateState()
        {
            base.UpdateState();
        }
    }
}
