﻿using UnityEngine;

public class MtcObjectManipulatorState : MonoBehaviour
{
    public MtcObjectManipulatorState()
    {
        Name = GetType().ToString();
    }
        

    protected string Name;

    public virtual void StateEnter()
    {
        Debug.Log("State Enter: " + Name);
    }

    public virtual void StateExit()
    {
        Debug.Log("State Exit: " + Name);
    }

    public virtual void UpdateState()
    {

    }
}
