﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.UX;
using UnityEngine;

namespace TheMTC.Adv_Vis.UX
{
    public class ScaleRig : MtcObjectManipulatorState
    {
        [HideInInspector]
        private GameObject target;
        public GameObject Target
        {
            get
            {
                return target;
            }

            set
            {
                target = value;

            }
        }

        enum HandleMode { Corner, Center, All };
        [SerializeField]
        private HandleMode handleMode;


        [Header("Customization Settings")]
        [SerializeField]
        private Material cornerScaleHandleMaterial;
        [SerializeField]
        private Material centerScaleHandleMaterial;

        [Header("Behavior")]
        [SerializeField]
        private float scaleRate = 1.0f;

        [SerializeField]
        [Tooltip("This is the maximum scale that one grab can accomplish.")]
        private float maxScale = 2.0f;

        [SerializeField]
        private float GizmoOffset = 0.2f;

        //TODO make a property
        public GameObject[] cornerHandles = null;

        public GameObject[] centerHandles = null;

        private List<Vector3> handleCentroids;

        private BoundingBoxGizmoHandle[] cornerRigScaleGizmoHandles;
        private BoundingBoxGizmoHandle[] centerRigScaleGizmoHandles;

        public Space Space;

        [SerializeField]
        private float scaleHandleSize = 0.04f;

        public Material ScaleHandleMaterial
        {
            get
            {
                return cornerScaleHandleMaterial;
            }

            set
            {
                cornerScaleHandleMaterial = value;
            }
        }

        public void FocusOnHandle(GameObject handle)
        {
            if (handle != null)
            {                
                for (int i = 0; i < cornerHandles.Length; ++i)
                {
                    cornerHandles[i].SetActive(cornerHandles[i].gameObject == handle);
                }
            }
            else
            {
                for (int i = 0; i < cornerHandles.Length; ++i)
                {
                    cornerHandles[i].SetActive(true);
                }
            }
        }

        private void Start()
        {

        }

        private void Update()
        {
            
        }                

        private void UpdateBoundsPoints()
        {
            handleCentroids = BoundingBoxRig.Instance.GetBounds();
        }

        private void UpdateHandles()
        {  
            void CreateCornerScaleHandles()
            {
                if (cornerHandles == null || cornerHandles.Length == 0)
                {
                    cornerHandles = new GameObject[handleCentroids.Count];
                    cornerRigScaleGizmoHandles = new BoundingBoxGizmoHandle[handleCentroids.Count];
                    for (int i = 0; i < cornerHandles.Length; ++i)
                    {
                        cornerHandles[i] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        cornerHandles[i].GetComponent<Renderer>().material = cornerScaleHandleMaterial;
                        cornerHandles[i].transform.localScale = scaleHandleSize * Vector3.one;
                        cornerHandles[i].name = "Corner " + i.ToString();
                        //cornerHandles[i].transform.parent = this.transform;
                        cornerHandles[i].transform.parent = this.transform;
                        cornerRigScaleGizmoHandles[i] = cornerHandles[i].AddComponent<BoundingBoxGizmoHandle>();
                        cornerRigScaleGizmoHandles[i].Rig = BoundingBoxRig.Instance;
                        cornerRigScaleGizmoHandles[i].ScaleRate = scaleRate;
                        cornerRigScaleGizmoHandles[i].MaxScale = maxScale;
                        cornerRigScaleGizmoHandles[i].TransformToAffect = Target.transform;
                        cornerRigScaleGizmoHandles[i].Axis = BoundingBoxGizmoHandleAxisToAffect.Y;
                    }
                }

                for (int i = 0; i < cornerHandles.Length; ++i)
                {
                    cornerHandles[i].transform.position = handleCentroids[i];
                    cornerHandles[i].transform.localRotation = Target.transform.rotation;
                }
            }

            void CreateCenterScaleHandles()
            {
                if (centerHandles == null || centerHandles.Length == 0)
                {
                    centerHandles = new GameObject[6];
                    centerRigScaleGizmoHandles = new BoundingBoxGizmoHandle[6];
                    for (int i = 0; i < centerHandles.Length; ++i)
                    {
                        centerHandles[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        centerHandles[i].GetComponent<Renderer>().material = centerScaleHandleMaterial;
                        centerHandles[i].transform.localScale = scaleHandleSize *  Vector3.one;
                        centerHandles[i].name = "Center " + i.ToString();
                        //CenterHandles[i].transform.parent = this.transform;
                        centerHandles[i].transform.parent = this.transform;
                        centerRigScaleGizmoHandles[i] = centerHandles[i].AddComponent<BoundingBoxGizmoHandle>();
                        centerRigScaleGizmoHandles[i].Rig = BoundingBoxRig.Instance;
                        centerRigScaleGizmoHandles[i].ScaleRate = scaleRate;
                        centerRigScaleGizmoHandles[i].MaxScale = maxScale;
                        centerRigScaleGizmoHandles[i].TransformToAffect = Target.transform;
                        centerRigScaleGizmoHandles[i].Axis = BoundingBoxGizmoHandleAxisToAffect.Y;
                        
                    }
                }

                centerHandles[0].transform.position = Target.transform.right * -GizmoOffset  + (handleCentroids[0] + handleCentroids[3]) * 0.5f;
                centerHandles[1].transform.position = Target.transform.right * GizmoOffset + (handleCentroids[4] + handleCentroids[7]) * 0.5f;
                centerHandles[2].transform.position = Target.transform.up * GizmoOffset + (handleCentroids[7] + handleCentroids[2]) * 0.5f;
                centerHandles[3].transform.position = Target.transform.up * -GizmoOffset + (handleCentroids[5] + handleCentroids[0]) * 0.5f;
                centerHandles[4].transform.position = Target.transform.forward * GizmoOffset + (handleCentroids[7] + handleCentroids[1]) * 0.5f;
                centerHandles[5].transform.position = Target.transform.forward * -GizmoOffset + (handleCentroids[6] + handleCentroids[0]) * 0.5f;

                foreach (var item in centerHandles)
                {
                    item.transform.localRotation = Target.transform.rotation;
                }

            }

            if (handleCentroids != null)
            {
                BoundingBoxRig.Instance.GetBounds();
            }

            if (handleMode == HandleMode.Corner || handleMode == HandleMode.All)
            {
                CreateCornerScaleHandles();
            }

            if (handleMode == HandleMode.Center || handleMode == HandleMode.All)
            {
                CreateCenterScaleHandles();
            }

        }

        private void ClearHandles()
        {
            if (cornerHandles != null)
            {
                for (int i = 0; i < cornerHandles.Length; ++i)
                {
                    GameObject.Destroy(cornerHandles[i]);
                }
                cornerHandles = null;
                handleCentroids = null;
            }

            if (centerHandles != null)
            {
                for (int i = 0; i < centerHandles.Length; ++i)
                {
                    GameObject.Destroy(centerHandles[i]);
                }
                centerHandles = null;
                handleCentroids = null;
            }

            cornerHandles = null;
            centerHandles = null;
            handleCentroids = null;
        }


        public override void StateEnter()
        {
            base.StateEnter();

            InputManager.Instance.RaiseBoundingBoxRigActivated(gameObject);
            BoundingBoxRig.Instance.ShowRig = true;

            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.Scale);
        }

        public override void StateExit()
        {
            base.StateExit();

            InputManager.Instance.RaiseBoundingBoxRigDeactivated(gameObject);
            BoundingBoxRig.Instance.ShowRig = false;

            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.none);

            ClearHandles();
        }

        public override void UpdateState()
        {
            base.UpdateState();

            UpdateBoundsPoints();
            UpdateHandles();
        }
    }
}
