﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TheMTC.Adv_Vis.UX
{
    public class PositionRig : MtcObjectManipulatorState
    {
        [HideInInspector]
        private GameObject target;
        public GameObject Target
        {
            get
            {
                return target;
            }

            set
            {
                target = value;
                foreach (var item in PositionHandles)
                {
                    
                    item.HostTransform = value.transform;
                }
            }
        }

        public List<AxisPositionGizmoHandle> PositionHandles;

        public Space Space; 

        // Use this for initialization
        void Start()
        {

            //Initialize references
            foreach (var item in PositionHandles)
            {
                item.PositionRig = this;                
            }
        }

        // Update is called once per frame
        void Update()
        {
           
        }
                       

        public override void StateEnter()
        {
            base.StateEnter();

            BoundingBoxRig.Instance.ShowRig = false;
            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.Position);
        }

        public override void StateExit()
        {
            base.StateExit();

            BoundingBoxRig.Instance.ShowRig = false;
            BoundingBoxRig.Instance.DisplayManipulationRig(BoundingBoxRig.ManipulationState.none);
        }

        public override void UpdateState()
        {
            base.UpdateState();

            if (target == null)
            {
                return;
            }

            this.transform.position = target.transform.position;

            if (Space == Space.World)
            {               
                this.transform.rotation = Quaternion.Euler(Vector3.zero);
            }
            if (Space == Space.Self)
            {
                this.transform.rotation = target.transform.rotation;
            }
                      
        }

    }
}
