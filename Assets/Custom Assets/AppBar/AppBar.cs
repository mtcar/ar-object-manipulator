﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TheMTC.Adv_Vis.UX
{
    /// <summary>
    /// Logic for the App Bar. Generates buttons, manages states.
    /// </summary>
    public class AppBar : MonoBehaviour
    {
        /// <summary>
        /// Where to display the app bar on the y axis
        /// This can be set to negative values
        /// to force the app bar to appear below the object
        /// </summary>
        public float HoverOffsetYScale = 0.25f;

        /// <summary>
        /// Pushes the app bar away from the object
        /// </summary>
        public float HoverOffsetZ = 0f;

        [SerializeField]
        [Tooltip("Uses an alternate follow style that works better for very oblong objects.")]
        private bool useTightFollow = false;

        /// <summary>
        /// Uses an alternate follow style that works better for very oblong objects
        /// </summary>
        public bool UseTightFollow
        {
            get { return useTightFollow; }
            set { useTightFollow = value; }
        }

        /// <summary>
        /// Class used for building toolbar buttons
        /// (not yet in use)
        /// </summary>
        [Serializable]
        public struct ButtonTemplate
        {
            //TODO Clean this Template
            public ButtonTemplate(ButtonTypeEnum type, string name, Sprite icon, string text/*, int defaultPosition, int manipulationPosition*/)
            {
                Type = type;
                Name = name;
                Icon = icon;
                Text = text;
                //DefaultPosition = defaultPosition;
                //ManipulationPosition = manipulationPosition;
            }

            public bool IsEmpty
            {
                get { return string.IsNullOrEmpty(Name); }
            }

            public string Name;
            //public int DefaultPosition;
            //public int ManipulationPosition;
            public ButtonTypeEnum Type;
            public Sprite Icon;
            public string Text;
        }

        [Flags]
        public enum ButtonTypeEnum
        {
            Custom = 0,
            Remove = 1,
            Adjust = 2,
            Hide = 4,
            Show = 8,
            Done = 16,
            Move = 32,
            Rotate = 64,
            Scale = 128,
            Undo = 256,
            Space = 512,
        }

        public enum AppBarDisplayTypeEnum
        {
            Manipulation,
            Standalone
        }

        public enum AppBarStateEnum
        {
            Default,
            Manipulation,
            Position,
            Rotate,
            Scale,
            Hidden
        }

        public BoundingBox BoundingBox
        {
            get { return boundingBox; }
            set { boundingBox = value; }
        }

        private BoundingBoxRig boundingRig;

        /// <summary>
        /// a reference to the boundingBoxRig that the appbar turns on and off
        /// </summary>
        public BoundingBoxRig BoundingRig
        {
            get { return boundingRig; }
            set { boundingRig = value; }
        }

        public GameObject SquareButtonPrefab;

        public List<AppBarButton> appBarInstantiatedButtonsList;

        public AppBarDisplayTypeEnum DisplayType = AppBarDisplayTypeEnum.Manipulation;

        public AppBarStateEnum State = AppBarStateEnum.Default;

        [SerializeField]
        private ButtonTemplate[] buttons;

        public ButtonTemplate[] Buttons
        {
            get { return buttons; }
            set { buttons = value; }
        }

        [SerializeField]
        private Transform buttonParent = null;

        [SerializeField]
        private BoundingBox boundingBox;

        private float lastTimeTapped = 0f;
        private float coolDownTime = 0.5f;
        public BoundingBoxHelper helper;

        public void Reset()
        {
            State = AppBarStateEnum.Default;
            FollowBoundingBox(false);
            lastTimeTapped = Time.time + coolDownTime;
        }

        public void Start()
        {
            State = AppBarStateEnum.Default;

            for (int i = 0; i < Buttons.Length; i++)
            {
                CreateButton(Buttons[i]);
            }

            helper = new BoundingBoxHelper();
        }

        


        public void AppBarButtonInputClicked(GameObject obj)
        {
            if (Time.time < lastTimeTapped + coolDownTime)
            {
                return;
            }

            lastTimeTapped = Time.time;



            switch (obj.name)
            {
                case "Remove":
                    // Destroy the target object, Bounding Box, Bounding Box Rig and App Bar

                    BoundingBoxRig.Instance.Deactivate();
                    BoundingBoxRig.Instance.Unbound();
                    BoundingBoxRig.Instance.SetState(BoundingBoxRig.ManipulationState.none);
                    break;

                case "Adjust":
                    // Make the bounding box active so users can manipulate it
                    State = AppBarStateEnum.Manipulation;
                    BoundingBoxRig.Instance.Activate();
                    BoundingBoxRig.Instance.SetState(BoundingBoxRig.ManipulationState.Selected);

                    break;

                case "Hide":
                    // Make the bounding box inactive and invisible
                    State = AppBarStateEnum.Hidden;
                    break;

                case "Show":
                    State = AppBarStateEnum.Default;
                    break;

                case "Done":
                    State = AppBarStateEnum.Default;
                    BoundingBoxRig.Instance.SetState(BoundingBoxRig.ManipulationState.Selected);
                    break;

                case "Move":
                    BoundingBoxRig.Instance.SetState(BoundingBoxRig.ManipulationState.Position);
                    State = AppBarStateEnum.Position;
                    break;

                case "Rotate":
                    BoundingBoxRig.Instance.SetState(BoundingBoxRig.ManipulationState.Rotation);
                    State = AppBarStateEnum.Rotate;
                    break;

                case "Scale":
                    BoundingBoxRig.Instance.SetState(BoundingBoxRig.ManipulationState.Scale);
                    State = AppBarStateEnum.Scale;
                    break;

                case "Space":
                    if (BoundingBoxRig.Instance.Space == Space.World)
                    {
                        BoundingBoxRig.Instance.Space = Space.Self;
                    }
                    else
                    {
                        BoundingBoxRig.Instance.Space = Space.World;
                    }

                    break;

                default:
                    break;
            }
        }
            

        private void CreateButton(ButtonTemplate template)
        {
            if (template.IsEmpty)
            {
                return;
            }

            GameObject newButton = Instantiate(SquareButtonPrefab, buttonParent);
            newButton.name = template.Name;
            newButton.transform.localPosition = Vector3.zero;
            newButton.transform.localRotation = Quaternion.identity;
            AppBarButton mtb = newButton.GetComponent<AppBarButton>();
            appBarInstantiatedButtonsList.Add(mtb);
            mtb.Initialize(this, template);
        }

        private void FollowBoundingBox(bool smooth)
        {
            if (boundingBox == null)
            {
                if (DisplayType == AppBarDisplayTypeEnum.Manipulation)
                {
                    // Hide our buttons
                    buttonParent.gameObject.SetActive(false);
                }
                else
                {
                    buttonParent.gameObject.SetActive(true);
                }
                return;
            }

            // Show our buttons
            buttonParent.gameObject.SetActive(true);

            //calculate best follow position for AppBar
            Vector3 finalPosition = Vector3.zero;
            Vector3 headPosition = Camera.main.transform.position;
            LayerMask ignoreLayers = new LayerMask();
            List<Vector3> boundsPoints = new List<Vector3>();
            if (boundingBox != null)
            {
                helper.UpdateNonAABoundingBoxCornerPositions(boundingBox.gameObject, boundsPoints, ignoreLayers);
                
                int followingFaceIndex = helper.GetIndexOfForwardFace(headPosition);
                Vector3 faceNormal = helper.GetFaceNormal(followingFaceIndex);

                //finally we have new position
                finalPosition = helper.GetFaceBottomCentroid(followingFaceIndex) + (faceNormal * HoverOffsetZ);
            }

            // Follow our bounding box
            transform.position = smooth ? Vector3.Lerp(transform.position, finalPosition, 0.5f) : finalPosition;

            // Rotate on the y axis
            Vector3 eulerAngles = Quaternion.LookRotation((boundingBox.TargetBoundsCenter  /*boundingBox.transform.position*/ - finalPosition).normalized, Vector3.up).eulerAngles;
            eulerAngles.x = 0f;
            eulerAngles.z = 0f;
            transform.eulerAngles = eulerAngles;
        }

        private void Update()
        {
            FollowBoundingBox(true);

            //We Update the status of each button
            foreach (var button in appBarInstantiatedButtonsList)
            {
                UpdateButton(button);
            }
        }


        protected void UpdateButton(AppBarButton button)
        {
            if (!button.initialized)
                return;

            switch (State)
            {
                case AppBar.AppBarStateEnum.Default:
                    // Show hide, adjust, remove buttons
                    // The rest are hidden
                    button.targetPosition = button.defaultOffset;
                    switch (button.template.Type)
                    {
                        case AppBar.ButtonTypeEnum.Hide:
                        case AppBar.ButtonTypeEnum.Remove:
                        case AppBar.ButtonTypeEnum.Adjust:
                        case AppBar.ButtonTypeEnum.Custom:
                            button.Show();
                            break;

                        default:
                            button.Hide();
                            break;
                    }
                    break;

                case AppBar.AppBarStateEnum.Hidden:
                    // Show show button
                    // The rest are hidden
                    button.targetPosition = button.hiddenOffset;
                    switch (button.template.Type)
                    {
                        case AppBar.ButtonTypeEnum.Show:
                            button.Show();
                            break;

                        default:
                            button.Hide();
                            break;
                    }
                    break;

                case AppBar.AppBarStateEnum.Manipulation:
                    // Show done / remove buttons
                    // The rest are hidden
                    button.targetPosition = button.manipulationOffset;
                    switch (button.template.Type)
                    {
                        case AppBar.ButtonTypeEnum.Rotate:
                        case AppBar.ButtonTypeEnum.Scale:
                        case AppBar.ButtonTypeEnum.Move:
                        case AppBar.ButtonTypeEnum.Space:
                            button.Show();
                            break;

                        default:
                            button.Hide();
                            break;
                    }
                    break;

                case AppBar.AppBarStateEnum.Position:
                    // Show done / remove buttons
                    // The rest are hidden
                    button.targetPosition = button.manipulationOffset;
                    switch (button.template.Type)
                    {
                        case AppBar.ButtonTypeEnum.Done:
                        case AppBar.ButtonTypeEnum.Undo:
                        case AppBar.ButtonTypeEnum.Space:
                            button.Show();
                            break;

                        default:
                            button.Hide();
                            break;
                    }
                    break;
                case AppBar.AppBarStateEnum.Scale:
                    // Show done / remove buttons
                    // The rest are hidden
                    button.targetPosition = button.manipulationOffset;
                    switch (button.template.Type)
                    {
                        case AppBar.ButtonTypeEnum.Done:
                        case AppBar.ButtonTypeEnum.Undo:
                        case AppBar.ButtonTypeEnum.Space:
                            button.Show();
                            break;

                        default:
                            button.Hide();
                            break;
                    }
                    break;
                case AppBar.AppBarStateEnum.Rotate:
                    // Show done / remove buttons
                    // The rest are hidden
                    button.targetPosition = button.manipulationOffset;
                    switch (button.template.Type)
                    {
                        case AppBar.ButtonTypeEnum.Done:
                        case AppBar.ButtonTypeEnum.Undo:
                        case AppBar.ButtonTypeEnum.Space:
                            button.Show();
                            break;

                        default:
                            button.Hide();
                            break;
                    }
                    break;
            }
        }
    }
}
