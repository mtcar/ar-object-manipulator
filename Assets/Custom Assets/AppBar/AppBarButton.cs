﻿using UnityEngine;
using UnityEngine.UI;

namespace TheMTC.Adv_Vis.UX
{
    /// <summary>
    /// Button logic for the App Bar. Determines position of the button in the App Bar, visibility based on the current mode.
    /// </summary>
    public class AppBarButton : MonoBehaviour
    {
        [SerializeField]
        private Text text;
        [SerializeField]
        private Image icon;

        //private ButtonIconProfile customIconProfile;
        public AppBar.ButtonTemplate template;
        public Vector3 targetPosition;
        public Vector3 defaultOffset;
        public Vector3 hiddenOffset;
        public Vector3 manipulationOffset;
        private UnityEngine.UI.Button cButton;
        private Renderer highlightMeshRenderer;
        //private CompoundButtonText text;
        //private CompoundButtonIcon icon;
        private AppBar parentToolBar;
        public bool initialized = false;

        public const float ButtonWidth = 0.12f;
        public const float ButtonDepth = 0.0001f;
        const float MoveSpeed = 5f;

        public void Initialize(AppBar newParentToolBar, AppBar.ButtonTemplate newTemplate)
        {
            void OnClickHandler()
            {               
               parentToolBar.AppBarButtonInputClicked(gameObject);
            }

            template = newTemplate;
            //customIconProfile = newCustomProfile;
            parentToolBar = newParentToolBar;

            text.text = template.Text;

            cButton = GetComponent<UnityEngine.UI.Button>();

           

            if (template.Icon == null)
            {
                icon.gameObject.SetActive(false);
            }
            else
            {
                icon.sprite = template.Icon;
            }

            cButton.onClick.AddListener(OnClickHandler);
            initialized = true;
            Hide();
        }
        
         void Update()
        {

        }

        public void Hide()
        {
            // cButton.interactable = false;
            this.gameObject.SetActive(false);
        }

        public void Show()
        {
            //  cButton.interactable = true;
            this.gameObject.SetActive(true);
        }
        
    }

   

}
