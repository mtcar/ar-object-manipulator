﻿using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.UX;
using UnityEngine;

#if UNITY_WSA && UNITY_2017_2_OR_NEWER
using UnityEngine.XR.WSA;
#endif

namespace TheMTC.Adv_Vis.UX
{
    /// <summary>
    /// Logic for the gizmo handles in Bounding Box
    /// </summary>
    public class BoundingBoxGizmoHandle : MonoBehaviour, IInputHandler, ISourceStateHandler
    {
        private BoundingBoxRig rig;
        private Transform transformToAffect;        
        private BoundingBoxGizmoHandleAxisToAffect axis;

        private Vector3 initialHandPosition;
        private Vector3 initialHandScreenPos;
        private Vector3 initialScale;
        private Vector3 initialPosition;
        private Vector3 initialScaleOrigin;
        private InputEventData inputDownEventData;
        private float minimumScaleNav = 0.001f;
        private float scaleRate = 1.0f;
        private float maxScale = 10.0f;
        private Vector3 lastHandWorldPos = Vector3.zero;

        public BoundingBoxGizmoHandleAxisToAffect Axis
        {
            get
            {
                return axis;
            }

            set
            {
                axis = value;
            }
        }

        public Transform TransformToAffect
        {
            get
            {
                return transformToAffect;
            }

            set
            {
                transformToAffect = value;
            }
        }

        public float ScaleRate
        {
            get
            {
                return scaleRate;
            }
            set
            {
                scaleRate = value;
            }
        }
        public float MaxScale
        {
            get
            {
                return maxScale;
            }
            set
            {
                maxScale = value;
            }
        }
        public BoundingBoxRig Rig
        {
            get
            {
                return rig;
            }

            set
            {
                rig = value;
            }
        }
        public bool RotateAroundPivot { get; set; }


        private void Start()
        {
            cachedRenderer = gameObject.GetComponent<Renderer>();
        }

        private void Update()
        {
            if (inputDownEventData != null)
            {
                Vector3 currentHandPosition = Vector3.zero;
                Quaternion currentHandOrientation = Quaternion.identity;

                //set values from hand
                currentHandPosition = GetHandPosition(inputDownEventData.SourceId);
                
                ApplyScale(currentHandPosition);

                lastHandWorldPos = currentHandPosition;
            }
        }

        private Vector3 GetHandPosition(uint sourceId)
        {
            Vector3 handPosition = new Vector3(0, 0, 0);
            inputDownEventData.InputSource.TryGetGripPosition(sourceId, out handPosition);
            return handPosition;
        }

        private void ApplyScale(Vector3 currentHandPosition)
        {
            if ((transformToAffect.position - initialHandPosition).magnitude > minimumScaleNav)
            {
                float scaleScalar = (currentHandPosition - transformToAffect.position).magnitude / (transformToAffect.position - initialHandPosition).magnitude;
                scaleScalar = Mathf.Pow(scaleScalar, scaleRate);
                Vector3 changeScale = new Vector3(scaleScalar, scaleScalar, scaleScalar);
                changeScale = GetBoundedScaleChange(changeScale);

                Vector3 newScale = changeScale;
                newScale.Scale(initialScale);

                //scale from object center
                transformToAffect.localScale = newScale;

                //now handle offset
                Vector3 currentScaleOrigin = initialScaleOrigin;
                currentScaleOrigin.Scale(changeScale);
                Vector3 postScaleOffset = currentScaleOrigin - initialScaleOrigin;

                //translate so that scale is effectively from opposite corner
                transformToAffect.position = initialPosition - postScaleOffset;
            }
        }

        private Vector3 GetBoundedScaleChange(Vector3 scale)
        {
            Vector3 maximumScale = new Vector3(initialScale.x * maxScale, initialScale.y * maxScale, initialScale.z * maxScale);
            Vector3 intendedFinalScale = new Vector3(initialScale.x, initialScale.y, initialScale.z);
            intendedFinalScale.Scale(scale);
            if (intendedFinalScale.x > maximumScale.x || intendedFinalScale.y > maximumScale.y || intendedFinalScale.z > maximumScale.z)
            {
                return new Vector3(maximumScale.x / initialScale.x, maximumScale.y / initialScale.y, maximumScale.z / initialScale.z);
            }

            return scale;
        }

        private Renderer cachedRenderer;
       

        private void ResetRigHandles()
        {
            inputDownEventData = null;
            

            cachedRenderer.sharedMaterial = Rig.ScaleHandleMaterial;

            HoloToolkit.Unity.InputModule.InputManager.Instance.PopModalInputHandler();
            //Rig.FocusOnHandle(null);
        }

        public void OnInputDown(InputEventData eventData)
        {
            inputDownEventData = eventData;

            initialHandPosition = GetHandPosition(eventData.SourceId);
            lastHandWorldPos = initialHandPosition;
            initialScale = transformToAffect.localScale;
            initialPosition = transformToAffect.position;

            initialScaleOrigin = transformToAffect.position - this.transform.position;
                      

            //Normalized
            initialHandScreenPos = new Vector3(initialHandScreenPos.x / Screen.width, (initialHandScreenPos.y / Screen.height) * (Screen.width / Screen.height), 0f);

            HoloToolkit.Unity.InputModule.InputManager.Instance.PushModalInputHandler(gameObject);

            cachedRenderer.sharedMaterial = Rig.InteractingMaterial;
           
            eventData.Use();
        }
        public void OnInputUp(InputEventData eventData)
        {
            inputDownEventData = null;
            ResetRigHandles();

            if (eventData != null)
            {
                eventData.Use();
            }
        }
        public void OnSourceDetected(SourceStateEventData eventData)
        {
        }

        public void OnSourceLost(SourceStateEventData eventData)
        {
            if ((inputDownEventData != null) &&
                (eventData.SourceId == inputDownEventData.SourceId))
            {
                inputDownEventData = null;
                ResetRigHandles();

                eventData.Use();
            }
        }
    }
}