﻿using HoloToolkit.Unity.InputModule;
using System.Collections.Generic;
using UnityEngine;

namespace TheMTC.Adv_Vis.UX
{
    /// <summary>
    /// Constructs the scale and rotate gizmo handles for the Bounding Box 
    /// </summary>
    public class BoundingBoxRig : MonoBehaviour
    {
        // Static singleton property
        public static BoundingBoxRig Instance { get; private set; }

        public List<MtcObjectManipulatorState> States;
        private MtcObjectManipulatorState _currentState;
        private int _index = 0;

        public enum ManipulationState {none, Selected, Position, Rotation, Scale, MADNESS };
        //TODO remove this
        public ManipulationState manipulationMode = ManipulationState.none;
                
        [SerializeField]
        private Space manipulationSpace;
        public Space Space { get => manipulationSpace; set {  manipulationSpace = value; PositionRig.Space = value; RotationRig.Space = value; ScaleRig.Space = value; } }

        [Header("Position Settings")]
        public bool dummyPositionSetting;


        [Header("Rotation Settings")]
        //Local-World Space Rotation
        //TODO this should be part of the rotate component
        [SerializeField]
        private Material rotateHandleMaterial;

        //TODO this should be part of the rotate component
        [SerializeField]
        private bool rotateAroundPivot = false;
      

        [Header("Scale Settings")]

        //TODO this should be part of the Scale component or be connected to that component
        [SerializeField]
        private Material scaleHandleMaterial;

        [SerializeField]
        private float scaleRate = 1.0f;

        [SerializeField]
        [Tooltip("This is the maximum scale that one grab can accomplish.")]
        private float maxScale = 2.0f;

        [SerializeField]
        private Material interactingMaterial;

        [Header("Preset Components")]
        [SerializeField]
        [Tooltip("To visualize the object bounding box, drop the MixedRealityToolkit/UX/Prefabs/BoundingBoxes/BoundingBoxBasic.prefab here.")]
        private BoundingBox boundingBoxPrefab;
      
        public BoundingBox BoundingBoxPrefab
        {
            get
            {
                return boundingBoxPrefab;
            }

            set
            {
                boundingBoxPrefab = value;
            }
        }
        public BoundingBox boxInstance;

        [HideInInspector]
        public GameObject objectToBound;

        private bool showRig = false;

      

      

       

        //public Camera projectionCamera;

        [SerializeField]
        private PositionRig         PositionRig;
        [SerializeField]
        private RotationSphereRig   RotationRig;
        [SerializeField]
        private ScaleRig            ScaleRig;

        //TODO Move to state
        public Material ScaleHandleMaterial
        {
            get
            {
                return scaleHandleMaterial;
            }

            set
            {
                scaleHandleMaterial = value;
            }
        }

        public Material RotateHandleMaterial
        {
            get
            {
                return rotateHandleMaterial;
            }

            set
            {
                rotateHandleMaterial = value;
            }
        }

        public Material InteractingMaterial
        {
            get
            {
                return interactingMaterial;
            }

            set
            {
                interactingMaterial = value;
            }
        }
        [Header("AppBar Configuration")]
        //[SerializeField]
        //[Tooltip("AppBar prefab.")]
        //private AppBar appBarPrefab = null;

        [SerializeField]
        private AppBar appBarInstance;
        [SerializeField]
        private float appBarHoverOffsetZ = 0.05f;

        void Awake()
        {
            // Save a reference to the BoundingBoxRig component as our singleton instance
            Instance = this;
        }


        public void Activate()
        {
            InputManager.Instance.RaiseBoundingBoxRigActivated(gameObject);
            ShowRig = true;
        }

        public void Deactivate()
        {
            InputManager.Instance.RaiseBoundingBoxRigDeactivated(gameObject);
            ShowRig = false;            
        }

        private void Start()
        {
            boxInstance = Instantiate(BoundingBoxPrefab) as BoundingBox;
            boxInstance.transform.parent = this.transform;

            //appBarInstance = Instantiate(appBarPrefab) as AppBar;
            appBarInstance.BoundingBox = boxInstance;
            appBarInstance.HoverOffsetZ = appBarHoverOffsetZ;
            Unbound();
            boxInstance.IsVisible = false;

            this.Space =  Space.World;

            //STATE MACHINE
            Debug.Log("State Manager: Start");

            if (_currentState == null)
            {
                _currentState = States[_index];
                _currentState.StateEnter();
            }
        }

        public void Update()
        {
            //STATE MACHINE
            if (_currentState == null)
                return;

            _currentState.UpdateState();
        }

        public void DisplayManipulationRig(ManipulationState manipulationMode)
        {
            switch (manipulationMode)
            {
                case ManipulationState.none:
                    PositionRig.gameObject.SetActive(false);
                    RotationRig.gameObject.SetActive(false);
                    ScaleRig.gameObject.SetActive(false);
                    break;
                case ManipulationState.Position:
                    PositionRig.gameObject.SetActive(true);
                    RotationRig.gameObject.SetActive(false);
                    ScaleRig.gameObject.SetActive(false);
                   
                    break;
                case ManipulationState.Rotation:
                    PositionRig.gameObject.SetActive(false);
                    RotationRig.gameObject.SetActive(true);
                    ScaleRig.gameObject.SetActive(false);
                    break;
                case ManipulationState.Scale:
                    PositionRig.gameObject.SetActive(false);
                    RotationRig.gameObject.SetActive(false);
                    ScaleRig.gameObject.SetActive(true);
                    break;
                case ManipulationState.MADNESS:
                    PositionRig.gameObject.SetActive(true);
                    RotationRig.gameObject.SetActive(true);
                    ScaleRig.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }


           
        }

      


        public void SetState(ManipulationState State)
        {
            Debug.Log("State Manager: GoToNextState");

            if (_currentState == null)
            {
                _currentState = States[(int)State];
                _currentState.StateEnter();
                return;
            }

            var prev = _currentState;
            _currentState = States[(int)State];
            prev.StateExit();
            manipulationMode = State;
            _currentState.StateEnter();
        }

        public bool ShowRig
        {
            get
            {
                return showRig;
            }
            set
            {
                if (boxInstance != null)
                {
                    boxInstance.IsVisible = value;
                }
                showRig = value;
            }
        }

        public List<Vector3> GetBounds()
        {
            if (objectToBound != null)
            {
                List<Vector3> bounds = new List<Vector3>();
                LayerMask mask = new LayerMask();

                GameObject clone = GameObject.Instantiate(boxInstance.gameObject);
                clone.transform.localRotation = Quaternion.identity;
                clone.transform.position = Vector3.zero;
                BoundingBox.GetMeshFilterBoundsPoints(clone, bounds, mask);
                Vector3 centroid = boxInstance.TargetBoundsCenter;
                GameObject.Destroy(clone);
#if UNITY_2017_1_OR_NEWER
                Matrix4x4 m = Matrix4x4.Rotate(objectToBound.transform.rotation);
                for (int i = 0; i < bounds.Count; ++i)
                {
                    bounds[i] = m.MultiplyPoint(bounds[i]);
                    bounds[i] += boxInstance.TargetBoundsCenter;
                }
#endif // UNITY_2017_1_OR_NEWER
                return bounds;
            }

            return null;
        }


        //Todo Convert into property so when NULL is the same as unselected
        public void SelecObject(GameObject target) {

            SetState(ManipulationState.Selected);

            //ClearHandles();           

            appBarInstance.gameObject.SetActive(true);
            
            objectToBound = target;
            boxInstance.Target = target;

            RotationRig.Target = target;
            ScaleRig.Target = target;            
            PositionRig.Target = target;
        }

        public void UnselectObject() {
            Unbound();

        }

        public void Unbound() {
            appBarInstance.gameObject.SetActive(false);
            RotationRig.gameObject.SetActive(false);
            //ClearHandles();
            //TODO : Hide boundingbox
        }      
    }













}